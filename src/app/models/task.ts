export class Task{

	constructor(
		public id : number,
		public name : string,
		public status : boolean,
		public description : string,
	){

	}

	getName(){
		return this.name.toUpperCase();
	}


}