import { Component, OnInit } from '@angular/core';
import { Task } from '../models/task';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {
	name: string = '';
	error : boolean = false;

	tasks = [
		new Task(1, "Nettoyer la maison", false),
		new Task(2, "Nettoyer la vaisselle", true),
		new Task(3, "Nettoyer la voiture", false),
	];


  	constructor() { }

  	ngOnInit() {
  	
  	}

  	incrementId() {
  		return this.tasks[this.tasks.length - 1].id + 1;
  	}


  	add() {
  		if (this.name.length > 3) {
  			this.error = false;
  			this.tasks.push(
  				new Task(5, this.name, false)
  			);
  			this.name = "";
  		} else{
  			this.error = true;
  		}
  	}

  	BoutonDes() {
  	document.getElementById("demo").innerHTML = "Hello World";
	}

}
